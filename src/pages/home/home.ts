import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FCM} from "@ionic-native/fcm";
import {Platform} from "ionic-angular";
import {Channel} from "./channel.model";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  loading = true;
  result = 'no result';
  data = null;
  timestamp: Date;

  channels: Channel[] = [new Channel('Mehr als Wohnen', 'mehrAlsWohnen', true)];

  url = 'https://www.homegate.ch/homegate-war/mieten/immobilien/trefferliste?a=e387&l=default&incsubs=1';
  regex = '<p><strong>(.*)</strong>';

  constructor(
    private http: HttpClient,
    private platform: Platform,
    private fcm: FCM,
    private storage: Storage
  ) {}

  ngOnInit(): void {
    this.fcm.getToken().catch(error => console.log(error));
    this.channels.forEach(channel => this.updateSubsciption(channel));
    this.platform.ready().then(() => {
      this.fcm.onNotification().subscribe( () => {
        location.reload();
      })
    });
    this.update();
  }

  private update() {
    this.loading = true;
    this.http.get(this.url, {responseType: 'text'}).subscribe(
      result => {
        this.result = this.parseResult(result);
        this.timestamp = new Date();
        this.loading = false;
      },
      () => {this.loading = false;}
    );
  }

  updateSubsciption(channel: Channel) {
    if (channel.activated) {
      this.fcm.subscribeToTopic(channel.topic)
        .then(() => this.storage.set(channel.topic, JSON.stringify(channel)))
        .catch(() => channel.activated = !channel.activated);

    } else {
      this.fcm.unsubscribeFromTopic(channel.topic)
        .then(() => this.storage.set(channel.topic, JSON.stringify(channel)))
        .catch(() => channel.activated = !channel.activated);
    }
  }

  private parseResult(result): string {
    return 'Gefundene Immobilien: ' + JSON.stringify(result).match(this.regex)[1];
  }
}
