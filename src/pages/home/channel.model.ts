export class Channel {
  constructor(
    public title: string = '',
    public topic: string = '',
    public activated: boolean = true
  ) {}
}
